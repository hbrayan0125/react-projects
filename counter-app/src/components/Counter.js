import React from "react";
import {useState} from 'react'
import '../App.css'

export default function Counter(){

    const [Count, setCount] = useState(0);

    return(
        <>
            <div className="flex justify-center items-center gap-10 flex-col h-full-100">
                <div className="w-2/6 flex flex-col items-center gap-10">
                    <h1 className="text-4xl font-medium">Counter App</h1>
                    <div className="flex items-center gap-10">
                        <button onClick={() => setCount(Count - 1)} className="bg-orange-500 border rounded-lg py-2 px-5 text-white hover:bg-orange-600" disabled={Count === 0}>Decrement</button>
                        <h1 className="text-3xl">{Count}</h1>
                        <button onClick={() => setCount(Count + 1)} className="bg-green-500 border rounded-lg py-2 px-5 text-white hover:bg-green-600">Increment</button>
                    </div>
                    <button onClick={() => setCount(0)} className="bg-gray-500 border rounded-lg py-2 px-5 text-white hover:bg-gray-600 w-4/6">Reset</button>
                </div>
            </div>
        </>
    )
}