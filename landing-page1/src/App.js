import './App.css';
import AppNavbar from './components/AppNavbar/AppNavbar.js';
import Benefits from './components/Benefits/Benefits';
import Gallery from './components/Gallery/Gallery';
import LandingPage from './components/LandingPage/LandingPage.js';
import Testimonials from './components/Testimonials/Testimonials';

function App() {
  return (
    <>
      <AppNavbar />
      <LandingPage />
      <Benefits />
      <Gallery />
      <Testimonials />
    </>
  );
}

export default App;
