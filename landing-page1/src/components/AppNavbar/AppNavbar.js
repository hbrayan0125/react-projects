import './AppNavbarCss.css'

export default function AppNavbar(){
    return (
        <>
            <div className="w-full p-6 bg-pink-400 shadow-md">
                <div className="flex justify-around items-center md:flex-row flex-col text-white">
                    <div>
                        <h1 className='font-medium text-2xl'>Cosmetics Website</h1>
                    </div>
                    <div className="mt-3 md:mt-0">
                        <ul className="flex gap-3 flex-col md:flex-row">
                            <li className="py-2 px-4 hover:border-b hover:cursor-pointer ease-in-out"><a href="#">Home</a></li>
                            <li className="py-2 px-4 hover:border-b hover:cursor-pointer ease-in-out"><a href="#">About</a></li>
                            <li className="py-2 px-4 hover:border-b hover:cursor-pointer ease-in-out"><a href="#">Services</a></li>
                            <li className="py-2 px-4 hover:border-b hover:cursor-pointer ease-in-out"><a href="#">Gallery</a></li>
                            <li className="py-2 px-4 hover:border-b hover:cursor-pointer ease-in-out"><a href="#">Testimonials</a></li>
                            <li className="py-2 px-4 hover:border-b hover:cursor-pointer ease-in-out"><a href="#">Features</a></li>
                            <li className="py-2 px-4 hover:border-b hover:cursor-pointer ease-in-out"><a href="#">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </>
    )
}