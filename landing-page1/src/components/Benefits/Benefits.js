import './Benefits.css';

export default function Benefits(){
    return (
        <>
            <div className="flex justify-center px-10 pb-10 pt-5 mb-20">
                <div className='h-full'>
                    <h1 className='text-center text-4xl font-bold my-10 text-pink-400'>Benefits of cosmetic products</h1>
                    <div className='grid lg:grid-cols-4 md:grid-cols-2 grid-cols-1  gap-5'>
                        <div className='shadow-md rounded-md hover:cursor-pointer hover-card'>
                            <img className='benefits-img-size' src="./images/benefits-images/Waterproof makeup-bro.svg" />
                            <h1 className='m-3 font-medium text-2xl text-pink-400 card-title'>Enhanced Appearance</h1>
                            <p className='mx-3 text-gray-700 card-paragraph'>
                            Craft compelling beauty product descriptions to entice customers, highlighting benefits and features while maintaining a captivating tone.
                            </p>
                            <button className='ml-3 border border-pink-400 card-button py-2 px-5 rounded-md my-5 text-pink-400'>Read More</button>
                        </div>
                        <div className='shadow-md rounded-md hover:cursor-pointer hover-card'>
                            <img className='benefits-img-size' src="./images/benefits-images/Instagram Video Streaming-cuate.svg" />
                            <h1 className='m-3 font-medium text-2xl text-pink-400 card-title'>Boosted Self-Esteem</h1>
                            <p className='mx-3 text-gray-700 card-paragraph'>
                            Cosmetics boost self-esteem by enhancing appearance, fostering confidence, and promoting a sense of attractiveness, contributing to improved self-assurance.
                            </p>
                            <button className='card-button ml-3 border border-pink-400 py-2 px-5 rounded-md my-5 text-pink-400'>Read More</button>
                        </div>
                        <div className='shadow-md rounded-md hover:cursor-pointer hover-card'>
                            <img className='benefits-img-size' src="./images/benefits-images/In the pool-pana.svg" />
                            <h1 className='m-3 font-medium text-2xl text-pink-400 card-title'>Protection from Harmful</h1>
                            <p className='mx-3 text-gray-700 card-paragraph'>
                            Certain cosmetics, such as sunscreen and UV-protective makeup, safeguard against UV rays, lowering the risk of skin damage and skin cancer.
                            </p>
                            <button className='card-button ml-3 border border-pink-400 py-2 px-5 rounded-md my-5 text-pink-400'>Read More</button>
                        </div>
                        <div className='shadow-md rounded-md hover:cursor-pointer hover-card'>
                            <img className='benefits-img-size' src="./images/benefits-images/Content creator-bro.svg" />
                            <h1 className='m-3 font-medium text-2xl text-pink-400 card-title'>Safety and Ease of Use</h1>
                            <p className='mx-3 text-gray-700 card-paragraph'>
                            Discover user-friendly cosmetics crafted for safety. Experience hassle-free application and enjoy a range of products designed with your well-being in mind.
                            </p>
                            <button className='card-button ml-3 border border-pink-400 py-2 px-5 rounded-md my-5 text-pink-400'>Read More</button>
                        </div>
                    </div>
                </div>
            </div>
            
        </>
    )
}