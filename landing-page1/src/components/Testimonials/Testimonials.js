import './Testimonials.css'

export default function Testimonials(){
    return (
        <>
            <div className="flex justify-center items-center flex-col mt-10 mb-20">
                <h1 className="text-pink-400 font-bold text-4xl mb-10">Testimonials</h1>
                <div className="w-5/6 grid grid-cols-1 lg:grid-cols-2 gap-8">
                    <div className="grid grid-cols-1 md:grid-cols-4 my-2 p-5 shadow-md rounded-md">
                        <div className='flex justify-center md:justify-left mb-2 md:mb-0'>
                            <img className='testimonials-image rounded-full' src="./images/testimonials-image/christopher-campbell-rDEOVtE7vOs-unsplash.jpg" />
                        </div>
                        <div className='px-5 flex flex-col col-span-3 text-center md:text-left'>
                            <h1 className='text-3xl font-medium text-pink-400 mb-2'>[Customer Name]</h1>
                            <p className=' text-gray-700'>
                            "I'm absolutely thrilled with the cosmetics products I purchased from <strong>Costemics Website</strong>. The quality is outstanding, and I've seen a remarkable improvement in my skin since using them. I can confidently say that these products have become an essential part of my daily beauty routine. Thank you, <strong>Costemics Website</strong>, for delivering such fantastic cosmetics!"
                            </p>
                        </div>
                    </div>
                    <div className="grid grid-cols-1 md:grid-cols-4 my-2 p-5 shadow-md rounded-md">
                        <div className='flex justify-center md:justify-left mb-2 md:mb-0'>
                            <img className='testimonials-image rounded-full' src="./images/testimonials-image/christian-buehner-DItYlc26zVI-unsplash.jpg" />
                        </div>
                        <div className='px-5 flex flex-col col-span-3 text-center md:text-left'>
                            <h1 className='text-3xl font-medium text-pink-400 mb-2'>[Customer Name]</h1>
                            <p className=' text-gray-700'>
                            "I've tried many cosmetics brands, but <strong>Costemics Website</strong> stands out. The range of products they offer is diverse, and the results are impressive. I particularly love their skincare line; it's been a game-changer for my complexion. I'm a loyal customer now, and I highly recommend <strong>Costemics Website</strong> to everyone looking for top-notch cosmetics."
                            </p>
                        </div>
                    </div>
                    <div className="grid grid-cols-1 md:grid-cols-4 my-2 p-5 shadow-md rounded-md">
                        <div className='flex justify-center md:justify-left mb-2 md:mb-0'>
                            <img className='testimonials-image rounded-full' src="./images/testimonials-image/foto-sushi-jKuch64WZ_o-unsplash.jpg" />
                        </div>
                        <div className='px-5 flex flex-col col-span-3 text-center md:text-left'>
                            <h1 className='text-3xl font-medium text-pink-400 mb-2'>[Makeup Artist Name]</h1>
                            <p className=' text-gray-700'>
                            "As a makeup artist, I rely on high-quality cosmetics, and <strong>Costemics Website</strong> never disappoints. Their products are not only long-lasting but also look stunning on clients. I trust their cosmetics for all my professional work. <strong>Costemics Website</strong> is a must-try for all beauty enthusiasts!"
                            </p>
                        </div>
                    </div>
                    <div className="grid grid-cols-1 md:grid-cols-4 my-2 p-5 shadow-md rounded-md">
                        <div className='flex justify-center md:justify-left mb-2 md:mb-0'>
                            <img className='testimonials-image rounded-full' src="./images/testimonials-image/sarah-brown-tTdC88_6a_I-unsplash.jpg" />
                        </div>
                        <div className='px-5 flex flex-col col-span-3 text-center md:text-left'>
                            <h1 className='text-3xl font-medium text-pink-400 mb-2'>[Customer Name]</h1>
                            <p className=' text-gray-700'>
                            "I've struggled with sensitive skin for years, but <strong>Costemics Website</strong> has been a game-changer. Their skincare range is gentle yet effective, and my skin has never looked better. I'm grateful for <strong>Costemics Website</strong> and their commitment to creating safe and exceptional cosmetics."
                            </p>
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </>
    )
}