import './LandingPage.css'


export default function LandingPage(){
    return (
        <>
           <div className="bg-pink-200 landing-max-height">
                <div className='w-full h-full grid md:grid-cols-2 grid-cols-1'>
                    <div className='flex justify-center items-center py-2'>
                        <div className='w-4/6 text-center text-white md:text-left lg:text-5xl text-4xl font-medium'>
                            Enhance Your Look with Premium Cosmetic Essentials to Discover Timeless Beauty.
                        </div>
                    </div>
                    <div className='flex justify-center items-center'>
                       <img className='img-md-size' src="./images/hero/Makeup artist-pana.svg" />
                    </div>
                </div>
           </div>
        </>
    )
}