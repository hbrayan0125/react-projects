import './Gallery.css'

export default function Gallery(){
    return (
        <>
            <div className="grid grid-cols-1 justify-center items-center mx-10 my-20">
                <h1 className="text-center text-pink-400 font-bold text-4xl mb-10">Gallery</h1>
                <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-5">
                    <img className='gallery-img-size' src="./images/gallery/element5-digital-ceWgSMd8rvQ-unsplash.jpg" />
                    <img className='gallery-img-size' src="./images/gallery/laura-chouette-FBlRYMG1DUc-unsplash.jpg" />
                    <img className='gallery-img-size' src="./images/gallery/laura-chouette-L0UsybxXiCs-unsplash.jpg" />
                    <img className='gallery-img-size' src="./images/gallery/laura-chouette-MQw3mB042aU-unsplash.jpg" />
                    <img className='gallery-img-size' src="./images/gallery/laura-chouette-VO3NIzjwAwE-unsplash.jpg" />
                    <img className='gallery-img-size' src="./images/gallery/raphael-lovaski-xQSim-3LgCQ-unsplash.jpg" />
                    <img className='gallery-img-size' src="./images/gallery/yawen-liao-lgag2swksqs-unsplash.jpg" />
                    <img className='gallery-img-size' src="./images/gallery/neauthy-skincare-8jg7vumdUlU-unsplash.jpg" />
                </div>
            </div>
        </>
    )
}