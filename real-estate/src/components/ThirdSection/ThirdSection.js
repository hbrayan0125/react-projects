import './ThirdSection.css'

export default function ThirdSection(){
    return (
        <>
            <div className='flex justify-center flex-col items-center'>
                <h1 className='text-white text-center text-5xl font-medium mb-10' data-aos="fade-up" data-aos-duration="1000" data-aos-once="true">See How Real Estate Website Can Help</h1>
                <div className="justify-center flex">
                    <div className="grid lg:grid-cols-3 md:grid-cols-2 grid-cols-1">   
                        <div className="card" data-aos="fade-up" data-aos-duration="1000" data-aos-once="true">
                            <p className="title">Buy a home</p>
                            <p className="text">With over 1 million+ homes for sale available on the website, Trulia can match you with a house you will want to call home.</p>
                        </div>
                        <div className="card" data-aos="fade-up" data-aos-duration="1500" data-aos-once="true">
                            <p className="title">Rent a home</p>
                            <p className="text">With 35+ filters and custom keyword search, Trulia can help you easily find a home or apartment for rent that you'll love.</p>
                        </div> 
                        <div className="card" data-aos="fade-up" data-aos-duration="2000" data-aos-once="true">
                            <p className="title">See neighborhoods</p>
                            <p className="text">With more neighborhood insights than any other real estate website, we've captured the color and diversity of communities.</p>
                        </div>  
                    </div>
                </div>
            </div>
        </>
    )
}