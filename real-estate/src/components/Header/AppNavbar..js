import React from "react";
import './AppNavbar.css'

export default function AppNavbar(){
    return (
        <>
            <div className="bg-image bg-max-size">
                <div className="text-white py-3">
                    <div className="flex justify-around items-center">
                        <div className="flex items-center gap-5">
                            <h1>REAL ESTATE WEBSITE</h1>
                            <ul className="flex justify-around">
                                <li className="px-5 py-2"><a href="#">BUY</a></li>
                                <li  className="px-5 py-2"><a href="#">RENT</a></li>
                                <li  className="px-5 py-2"><a href="#">SELL</a></li>
                            </ul>
                        </div>
                        <div className="flex justify-around items-center gap-5">
                            <h1>+234-4566-345</h1>
                            <ul className="flex justify-around">
                                <li  className="px-5 py-2"><a href="#">SIGN UP</a></li>
                                <li  className="px-5 py-2"><a href="#">LOG IN</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}