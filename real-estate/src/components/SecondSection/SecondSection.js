import React from 'react';
import './SecondSection.css';

export default function SecondSection(){
    return (
        <>
            <div className="text-center my-20">   
                <h1 className='font-medium text-white text-5xl mb-2' data-aos="fade-up" data-aos-duration="1000" data-aos-once="true">Explore Homes On Real Estate Website</h1>
                <p className='text-2xl text-white opacity-70' data-aos-once="true" data-aos="fade-up" data-aos-duration="1300">Take a deep dive and browse homes for sale, original neighborhood photos, <br /> resident reviews and local insights to find what is right for you. </p>
            </div>
            
        </>
    )
}