


export default function FourthSection(){
    return (
        <>  
            <div className="flex justify-center my-20">
                <div>
                    <div className="flex flex-col items-center">
                        <h1 className="text-white font-medium text-5xl mb-2">The Perfect Place to Manage Your Property </h1>
                        <p className="text-2xl text-white opacity-70" data-aos-once="true" data-aos="fade-up" data-aos-duration="1300">
                            Work with the best suite of property management tools on the market.
                        </p>
                    </div>
                    <div>
                        <di>
                            <h1>
                                Tips for Renters
                            </h1>
                        </di>
                    </div>
                </div>
               
            </div>
        </>
    )
}