import './Hero.css';
import AOS from 'aos';
import 'aos/dist/aos.css'
import { useEffect } from 'react';

export default function Hero(){
    useEffect(() => {
        AOS.init();
    })
    return (
        <>
            <div className='absolute w-4/6 text-white'>
                <div className='flex justify-center flex-col'>
                    <h1 className='font-medium lg:text-8xl md:text-7xl text-5xl text-center' data-aos-once="true" data-aos="fade-up" data-aos-duration="1000">Discover a place </h1>
                    <h1 className='font-medium lg:text-8xl md:text-7xl text-5xl text-center' data-aos-once="true" data-aos="fade-up" data-aos-duration="1300">you’ll love to live</h1>
                </div>
            </div>
        </>
    )
}