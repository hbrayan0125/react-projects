import FourthSection from "../components/FourthSection/FourthSection";
import AppNavbar from "../components/Header/AppNavbar.";
import Hero from "../components/HeroSection/Hero";
import SecondSection from "../components/SecondSection/SecondSection";
import ThirdSection from "../components/ThirdSection/ThirdSection";

export default function Home(){
    return (
        <>  
            <AppNavbar />
            <Hero />
            <SecondSection />
            <ThirdSection />
            <FourthSection />
        </>
    )
}