import './App.css';
import Header from './pages/Home';
import Hero from './components/HeroSection/Hero';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

function App() {


  return (
    <>
        <Router>
          <Routes>
            <Route path='/' element={<Header />} />
          </Routes>
        </Router>
    </>
  );  
}

export default App;
