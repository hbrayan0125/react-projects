import React, {useState} from "react";
import '../App.css'

export default function TodoList(){
    const [todos, setTodos] = useState([]);
    const [AddTodo, setAddTodo] = useState("");

    const CreateTodo = () => {
        setTodos([...todos, AddTodo]);
        console.log(todos)
    }

    return (
        <>
            {/* <div className="flex justify-center items-center" style={{height: "100vh"}}>
                <div className="grid grid-col-1 gap-5 w-2/6 border h-3/4">
                    <h1 className="text-center text-4xl font-medium">Todo List</h1>
                    <div className="flex justify-center gap-5 h-1/4">
                        <input type="text" onChange={(e) => setAddTodo(e.target.value)} className=" input-type bg-gray-200 border rounded-md px-2 py-3" placeholder="Enter todo"/>
                        <button onClick={CreateTodo} className="py-2 px-5 bg-blue-500 font-bold text-white border rounded-lg">Add</button>
                    </div>
                    <div className="w-full px-10 flex items-center flex-col">
                        {   todos.map((todo, index) => (
                            <div key={index} className="bg-gray-200 py-2 px-5 w-full border rounded-md flex items-center justify-between mb-2">
                                <p className="text-2xl">{todo}</p>
                                <div className="flex gap-2">
                                    <button className="bg-orange-500 hover:bg-orange-600 py-2 px-5 text-white border rounded-md">Edit</button>
                                    <button className="bg-red-500 hover:bg-red-600 py-2 px-5 text-white border rounded-md">Delete</button>
                                </div>
                            </div>
                            ))
                        }
                    </div>
                </div>
            </div> */}
            <div className="border bg-blue-400 flex justify-center items-center" style={{height: "100vh"}}>
                <div className="border w-5/6 md:w-4/6 lg:w-2/6 h-3/6 p-5">
                    <div className="border h-full">
                        <h1 className="text-center text-white text-3xl py-3">Create Task</h1>
                        <div className="w-full grid grid-cols-3 gap-2">
                            <input type="text" onChange={(e) => setAddTodo(e.target.value)} className="border px-2 rounded-md col-span-2 bg-gray-200" placeholder="Create Task" />
                            <button onClick={CreateTodo} className="bg-blue-500 text-white py-2 px-5 rounded">Create</button>
                        </div>
                        {
                            todos.map((todo, index) => (
                                <div className="w-full mt-5" key={index}>
                                    <div className="grid grid-cols-4 gap-2 items-center">
                                        <h1 className="col-span-2 text-3xl">{todo}</h1>
                                        <div className="flex col-span-2 justify-end gap-2">
                                            <button className="bg-orange-500 py-2 px-5 rounded-md text-white">Edit</button>
                                            <button className="bg-red-500 py-2 px-5 rounded-md text-white">Delete</button>
                                        </div>
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>
        </>
    )
}